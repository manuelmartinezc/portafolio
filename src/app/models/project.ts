export class Project {
    constructor(
        public _id:string,
        public name: string,
        public description: string,
        public category: string,
        public year: number,
        public langs: string,
        public image: string
    ){

        // Deves inicializar el objeto para que no tengas problemas
        _id= "";
        name= "";
        description= "";
        category= "";
        year= 0;
        langs= "";
        image= "";
    }
}