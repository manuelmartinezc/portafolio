import { Component, OnInit } from '@angular/core';
import {Project} from '../../models/project';
import{ProjectService} from '../../services/project.service'
//import { TouchSequence } from 'selenium-webdriver';
import{UploadService} from '../../services/upload.service'
import {Global} from '../../services/global'
import { StringifyOptions } from 'querystring';
import {  Router, ActivatedRoute, Params } from '@angular/router';



@Component({
  selector: 'app-edit',
  templateUrl: '../create/create.component.html',
  styleUrls: ['./edit.component.css'],
  providers:[ProjectService, UploadService]
})
export class EditComponent implements OnInit {
  public title: string;
  public project: Project;
  public save_project;
  public status: string;
  public filesToUpload: Array<File>;
  public url: string;
  

  constructor(
    private _projectService: ProjectService,
    private _uploadService: UploadService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.title = "Editar Projecto";
    this.url = Global.url;
   }
  

   ngOnInit() {
    this._route.params.subscribe(parmas =>{
      let id = parmas.id;

      this.getProject(id);
    });
  }

  getProject(id){
     this._projectService.getProject(id).subscribe(
       response =>{
         this.project = response.project;
       },
       error =>{
        console.log(error);
       }
     );
  }

  onSubmit(){
    this._projectService.updateProject(this.project).subscribe(
      response =>{
        if(typeof response.project != 'undefined') {
             
         if (this.filesToUpload){
            //subir la imagen
          this._uploadService.makeFileRequest(Global.url+"upload-image/"+response.project._id, [], this.filesToUpload, 'image').
          then((result:any)=>{
            this.save_project = result.project;
            console.log(result);
            this.status = "success";
          
         });

         }else{
          this.save_project = response.project;
          this.status = "success";
         }

       }
       else {
         this.status = "failed";
       }

      },
      error =>{

      }
    );
  }

  fileChangeEvent(FileInput: any){
    //console.log(FileInput);
    this.filesToUpload = <Array <File>>FileInput.target.files;
  }

}
