import { Component, OnInit } from '@angular/core';
import {Project} from '../../models/project';
import{ProjectService} from '../../services/project.service'
//import { TouchSequence } from 'selenium-webdriver';
import{UploadService} from '../../services/upload.service'
import {Global} from '../../services/global'
import { StringifyOptions } from 'querystring';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers:[ProjectService, UploadService]
})

export class CreateComponent implements OnInit {
  public title: string;
  public project: Project;
  public save_project;
  public status: string;
  public filesToUpload: Array<File>;
  

  constructor(
    private _projectService: ProjectService,
    private _uploadService: UploadService
  ) {
    this.title = "Crear proyecto";
    this.project = new Project("","","","",2019,"","");
   }

  ngOnInit() {
  }

  onSubmit(form) {
      console.log(this.project);
      this.status= "";
      

      //guardar los datos
      this._projectService.saveProject(this.project).subscribe(
        response => {
          
        

          console.log(response);
           if(typeof response.project != 'undefined') {
             
            if (this.filesToUpload){
              //subir la imagen
              this._uploadService.makeFileRequest(Global.url+"upload-image/"+response.project._id, [], this.filesToUpload, 'image').
              then((result:any)=>{
                this.save_project = result.project;
                console.log(result);
                this.status = "success";
                form.reset();
              
             });
            }else{
              this.save_project = response.project;
              this.status = "success";
              form.reset();
              
            }
           }
           else {
             this.status = "failed";
           }

           console.log(this.status);
        },
        err => {
          console.log(err);
          console.log("^^ err");
      });
  }

  fileChangeEvent(FileInput: any){
    //console.log(FileInput);
    this.filesToUpload = <Array <File>>FileInput.target.files;
  }

}
